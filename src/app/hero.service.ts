import { Injectable } from '@angular/core';
import { Hero } from './hero'; 
import { HEROES } from './mock-heroes';
import { Observable , of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
  providedIn: 'root'
})
export class HeroService {
    constructor(
        private http: HttpClient,
        private messageService: MessageService
    ) { }
    private heroesUrl = 'api/heroes';

    getHeroes(): Observable<Hero[]>{
        this.messageService.add('HeroService: fetched heroes');
        return this.http.get<Hero[]>(this.heroesUrl)
            .pipe(
                tap(heroes => this.log('fetch heroes')),
                catchError(this.handleError('getHeroes', []))
            );
    }
     /** GET hero by id. Return `undefined` when id not found */ 
    getHeroNo404<Data>(id: number): Observable<Hero>{
        const url = `${this.heroesUrl}/?id=${id}`;
        this.messageService.add(`HeroService: fetched hero id=${ id }`);
        return this.http.get<Hero[]>(url)
            .pipe(
                map(
                    //get the first element in the arrya and pass down
                    heroes => heroes[0],
                    tap(h => {
                        const outcome = h ? 'fetched' : 'did not find';
                        this.log(`${outcome} hero id=${id}`);
                    }),
                    catchError(this.handleError<Hero>(`getHero id=${id}`))
                )
            );
    }
    // even only need string, but still pass object
    addHero(hero: Hero): Observable<Hero>{
        return this.http.post<Hero>(this.heroesUrl, hero, httpOptions).pipe(
            tap((hero: Hero) => this.log(`added hero id=${hero.id}`)),
            catchError(this.handleError<Hero>(`addHero`))
        );
    }


    updateHero(hero: Hero): Observable<any>{
        this.messageService.add(`HeroService: update hero id=${ hero.id }`);
        return this.http.put(this.heroesUrl, hero, httpOptions).pipe(
            tap(_ => this.log(`updated hero id=${hero.id}`)),
            catchError(this.handleError<any>(`update hero`))
        );
    }

    deleteHero(hero: Hero | number): Observable<Hero>{
        const id = typeof hero === 'number' ? hero : hero.id;
        this.messageService.add(`DeleteService: delete hero id=${id }`);
        const url = `${this.heroesUrl}/${id}`;
        return this.http.delete(url, httpOptions).pipe(
            tap(_ => this.log(`delete hero id=${id}`)),
            catchError(this.handleError<Hero>(`delete hero`))
        );
    }

    searchHeroes(term: string): Observable<Hero[]>{
        if(!term.trim()) return of([]);
        return this.http.get<Hero []>(`${this.heroesUrl}/?name=${term}`)
            .pipe(
                tap(heroes => this.log('searchHeroes heroes')),
                catchError(this.handleError<Hero []>('searchHeroes', []))
        );
    }

    private handleError<T>(operation = 'operation', result? : T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error);
            // TODO: should transfer error for user consumpution
            this.log(`${ operation } failed: ${error.message}`);
            //let app running by returning empty result
            return of(result as T)
        }
    }

    private log(message: string) {
        this.messageService.add('HeroService: ' + message);
    }
}
