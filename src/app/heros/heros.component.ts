import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heros.component.html',
  styleUrls: ['./heros.component.css']
})
export class HerosComponent implements OnInit {
    heroes: Hero[];

    constructor(private heroService: HeroService) { 
    }

    ngOnInit() {
        this.getHeroes();
    }
    getHeroes(): void {
        this.heroService.getHeroes()
            .subscribe(heroes => this.heroes = heroes);
    }

    addHero(name: string): void {
        name = name.trim();
        if(!name) return;
        //create a Hero like object from the name and pass it to the service
        this.heroService.addHero({ name } as Hero)
            .subscribe(hero => {
                this.heroes.push(hero);
            });
    }

    deleteHero(hero: Hero): void {
        this.heroes = this.heroes.filter(h => h !== hero);
        //even we don't use any data returned from the server
        // we still need subscribe, otherwise the oberservable will not work
        this.heroService.deleteHero(hero).subscribe();
    }

}
