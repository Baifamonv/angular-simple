import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import {
    debounceTime, 
    distinctUntilChanged,
    switchMap,
} from 'rxjs/operators';

import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.css']
})
export class HeroSearchComponent implements OnInit {
    heroes$: Observable<Hero []>;

    private searchTerms = new Subject<string>();

    constructor(private heroService: HeroService) { }

    search(term: string): void {
        this.searchTerms.next(term);
    }
    ngOnInit(): void {
        this.heroes$ = this.searchTerms.pipe(
            //make sure request to search will not more frequently than 300ms
            debounceTime(300),
            // request only sent if the filter text changed
            distinctUntilChanged(),
            //call the search service for each search term that makes it through
            // debounce and distinctUntilChanged
            switchMap((term: string) =>
                this.heroService.searchHeroes(term)),
        );
    }

}
